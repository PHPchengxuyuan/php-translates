<?php
/*
 * @description: 抽象产品接口类
 * @Author: 1351218627@qq.com
 * @Date: 2021-05-14 19:09:41
 */

namespace app\translates\translate;

interface ITranslate
{
    /**
     * 执行方法
     */
    public function translate();
}
